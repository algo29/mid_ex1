
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;




/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADMIN
 */
public class Mid_ex1 {
    public static void main(String[] args) {
        long start = System.nanoTime();
        try {
            // you can change file text name
            //Ex. 
            Path file = Paths.get("D:\\Algo\\Mid_ex1\\ex1_files\\4.in.txt");
            BufferedReader reader = Files.newBufferedReader(file, StandardCharsets.UTF_8);
            String line;
            while(( line = reader.readLine()) != null){
            String S = line;
        
            
           
            if(Integer.valueOf(S)>=0 && Integer.valueOf(S)<=9){
                int number = Integer.parseInt(S);
                System.out.println("S to int is:"+number);
                System.err.println("Check number from String to Integer ");
                System.err.print("number + number :" );
                System.err.println(number + number);
            }else{
                System.err.println("Out of range");
            }
            }
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(Mid_ex1.class.getName()).log(Level.SEVERE, null, ex);
        }
        long end = System.nanoTime();
        System.out.println("Running Time of Algorithm is : "+(end-start) * 1E-9+ "secs.");
        }
   
    }

